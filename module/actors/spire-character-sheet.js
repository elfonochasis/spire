export class SpireCharacterSheet extends ActorSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["spire", "sheet", "character"],
            template: "systems/spire/templates/actors/spire-character-sheet.html",
        });
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
        if (!this.options.editable) {
            return;
        }

        // Add item
        html.find(".spire-item-create").click(this._onItemCreate.bind(this));

        // Edit item
        html.find(".spire-item-edit").click((ev) => {
            const boxItem = $(ev.currentTarget).parents(".spire-table-item");
            const item = this.actor.items.get(boxItem.data("itemId"));
            item.sheet.render(true);
        });

        // Delete item
        html.find(".spire-item-delete").click((ev) => {
            const boxItem = $(ev.currentTarget).parents(".spire-table-item");
            const item = this.actor.items.get(boxItem.data("itemId"));
            item.delete();
        });
    }

    /** @override */
    getData() {
        const data = super.getData();

        // Prepare items
        if (this.actor.data.type == "character") {
            this._prepareCharacterItems(data);
        }

        return data;
    }

    _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        const type = header.dataset.type;
        const data = duplicate(header.dataset);
        const name = `New ${type.capitalize()}`;
        const itemData = {
            name: name,
            type: type,
            data: data,
        };
        delete itemData.data["type"];
        return this.actor.createEmbeddedDocuments("Item", [itemData]).then((items) => items[0].sheet.render(true));
    }

    _prepareCharacterItems(sheetData) {
        const actorData = sheetData.actor;

        // Initialize containers.
        const abilities = [];
        const bonds = [];
        const equipments = [];
        const fallouts = [];
        const knacks = [];
        const refreshes = [];

        // Iterate through items, allocating to containers
        for (let i of sheetData.items) {
            i.img = i.img || DEFAULT_TOKEN;

            if (i.type === "ability") {
                abilities.push(i);
            } else if (i.type === "bond") {
                bonds.push(i);
            } else if (i.type === "knack") {
                knacks.push(i);
            } else if (i.type === "equipment") {
                equipments.push(i);
            } else if (i.type === "fallout") {
                fallouts.push(i);
            } else if (i.type === "refresh") {
                refreshes.push(i);
            }
        }

        // Assign and return
        actorData.abilities = abilities;
        actorData.bonds = bonds;
        actorData.equipments = equipments;
        actorData.fallouts = fallouts;
        actorData.knacks = knacks;
        actorData.refreshes = refreshes;
    }
}
